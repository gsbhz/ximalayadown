# 西宾喜马拉雅语音下载工具（ximalayadown）

## 一、介绍
   
   西宾喜马拉雅语音下载工具，能够帮助你下载喜马拉雅中的音频节目。如果你是会员，它还能帮你下载会员节目。

## 二、下载地址
   
   本站下载：[西宾喜马拉雅语音下载工具（ximalayadown）](https://gitee.com/xibinhui/ximalayadown/tree/master/bin)
   
   百度网盘下载：[西宾喜马拉雅语音下载工具（ximalayadown）](https://pan.baidu.com/s/1QCsV62aI_hOenw4-f0Ocqw?pwd=xibh#list/path=%2Fxibinhui%2Fximalayadown)

## 三、安装教程
1. 将下载的文件解压到D:\xibinhui，D:\Program\xibinhui或者其他你规划的目录中。
   
   注意事项：

   ​   1. 请不要解压到桌面。

   ​   2. 请不要解压到系统所在的盘符中。

   ​   3. 请不要在压缩文件中运行本程序。  


2. 我的安装路径为D:\Program\xibinhui\ximalayadown，如下图所示：
   
   ![](https://gitee.com/xibinhui/ximalayadown/raw/master/doc/001.png)

3. 安装完成。双击ximalayadown.exe即可打开本程序。

## 四、使用说明
1. **使用本程序打开喜马拉雅首页，登录喜马拉雅。**
   
   ![](https://gitee.com/xibinhui/ximalayadown/raw/master/doc/002.png)
   
   程序不会缓存登录信息，退出后需要重新登录。不登录的话，无法下载你作为会员能够访问的课程。本程序不会记录你的用户名和密码，作者也没这能力。你可以放心使用。建议你扫码登录，扫码登录不涉及用户名和密码，可解瓜田李下之嫌。

2. **在页面中找到你感兴趣的音频节目列表。**
   
   节目列表的URL形如下图：

   ![](https://gitee.com/xibinhui/ximalayadown/raw/master/doc/004.png)

   节目列表的界面如下图所示：

   ![](https://gitee.com/xibinhui/ximalayadown/raw/master/doc/003.png)

3. **下载节目中的音频。**
   
   导航到节目目录页面中，等待页面加载完成后，如下图所示：
  
   ![](https://gitee.com/xibinhui/ximalayadown/raw/master/doc/005.png)
   
   点击程序右下角的“音频目录”按钮。稍等片刻，等待“下载任务列表”出现你要下载的课程目录任务。如下图所示：
   
   ![](https://gitee.com/xibinhui/ximalayadown/raw/master/doc/006.png)
   
   ![](https://gitee.com/xibinhui/ximalayadown/raw/master/doc/007.png)
   
   “下载任务列表”出现你要下载的课程目录任务后，你可以进行其他操作了。

4. **请耐心等待下载完成**。
   
   下载的文件如下图所示：
   
   ![](https://gitee.com/xibinhui/ximalayadown/raw/master/doc/008.png)
   
   ​好了，完成以上步骤，你就可以下载一个完整的节目了。在有任务异常时，你可以在“下载任务列表”中点击“重启选择的任务”，“停止选择的任务”进行单任务下载。

5. **结束语**
   
   至此，工具的主要功能介绍完了，希望工具能够帮你省点时间。

## 五、配置说明（使用默认即可）
   
   ![](https://gitee.com/xibinhui/ximalayadown/raw/master/doc/009.png)

1. 文件保存目录
   
   下载的音频文件保存在这里。

2. 同时下载的最大任务数
   
   建议使用默认配置。细水长流、安全第一。

3. 点击下载后显示下载任务列表窗口
   
   点击程序底部的几个下载按钮后，自动显示任务列表窗口。

4. 点击下载后提示处理的注意信息
   
   点击程序底部的几个下载按钮后，提示注意的信息。一般使用本软件一次后，就可关闭提示。

5. 允许添加重复任务
   
   在你确实需要重复下载时，可以打开本配置。

6. 所有任务结束后关机
   
   运行期间的配置，不会写入配置文件中。程序重启后会默认为关闭。

## 六、下载任务列表的说明 
   
   ​下载任务列表中的任务会在程序退出时，保存为文件，程序启动时会加载任务，但是不会自动执行。任务过多时会影响程序性能，建议定期删除已经完成的任务。

## 七、使用小技巧 
   
   ​喜马拉雅有流控，每天下载的节目数量有限。当界面出现任务下载失败后，在日志窗口中看到了“操作太频繁”的字样，说明你被流控了。这种情况下，你可以关闭程序第二天继续下载。方法步骤：

   1. 关闭程序。程序会保存任务列表中的任务。
   2. 第二天。
   3. 打开程序，扫码登录喜马拉雅。不扫码登录，无法下载你作为会员能听的节目。
   4. 打开“任务列表”窗口，在“任务列表”窗口中点击“启动顺序下载”。
   5. 程序会从昨天失败的任务开始继续下载。下载成功的任务，不会重复下载。
   6. 节目下载完整后，你就可以删除已经完成的任务了。
   7. 删除任务重新添加后，程序会覆盖已经存在的文件，重新下载。

## 八、版本
#### v1.7（2022-10-12）
   1. 新增文件存在时跳过下载的选项。
   2. 新增任务间休眠的选项。
   3. 为减少流控，将任务数固定为1。
   
#### v1.6（2022-08-08）
   1. 修复一些BUG。
   
#### v1.5（2022-07-30）
   1. 修复程序关闭时，任务不保存的BUG。
	
#### v1.4（2022-06-13）
   1. 修复一些BUG。
   
#### v1.3（2022-06-09）
   1. 修复倒序目录无法解析全部节目的BUG。
	
#### v1.2（2022-06-08）
   1. 修复下载完成后不关机的BUG。
   2. 增加删除选择的任务。

#### v1.1（2022-06-07）
   1. 增加网络异常时，重试的机制。
   2. 增加重新执行错误任务的机制。
   3. 修复无法下载会员节目的BUG。

#### v1.0（2022-06-05）
   1. 发布1.0。
   2. 祝端午节快乐！

## 九、支持
   1. 软件问题：在线论坛 - [Issues](https://gitee.com/xibinhui/ximalayadown/issues)
   2. 功能需求：邮箱 - leashi@163.com
   3. 支付故事：QQ号 - 79534262  QQ群 - 314714697